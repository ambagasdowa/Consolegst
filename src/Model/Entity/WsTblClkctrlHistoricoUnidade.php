<?php
namespace Consolegst\Model\Entity;

use Cake\ORM\Entity;

/**
 * WsTblClkctrlHistoricoUnidade Entity
 *
 * @property int $id
 * @property string|null $block_desc
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $status
 */
class WsTblClkctrlHistoricoUnidade extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'block_desc' => true,
        'created' => true,
        'modified' => true,
        'status' => true
    ];
}
