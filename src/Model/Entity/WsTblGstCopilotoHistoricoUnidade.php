<?php
namespace Consolegst\Model\Entity;

use Cake\ORM\Entity;

/**
 * WsTblGstCopilotoHistoricoUnidade Entity
 *
 * @property int $id
 * @property string|null $alias
 * @property string|null $nombre
 * @property int|null $idMovil
 * @property float|null $latitud
 * @property float|null $longitud
 * @property string|null $lugar
 * @property string|null $placas
 * @property \Cake\I18n\FrozenTime|null $fechahorautc
 * @property int|null $tipo
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $status
 */
class WsTblGstCopilotoHistoricoUnidade extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'alias' => true,
        'nombre' => true,
        'idMovil' => true,
        'latitud' => true,
        'longitud' => true,
        'lugar' => true,
        'placas' => true,
        'fechahorautc' => true,
        'tipo' => true,
        'created' => true,
        'modified' => true,
        'status' => true
    ];
}
