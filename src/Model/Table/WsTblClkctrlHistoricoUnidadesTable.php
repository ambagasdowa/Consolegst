<?php
namespace Consolegst\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WsTblClkctrlHistoricoUnidades Model
 *
 * @method \Consolegst\Model\Entity\WsTblClkctrlHistoricoUnidade get($primaryKey, $options = [])
 * @method \Consolegst\Model\Entity\WsTblClkctrlHistoricoUnidade newEntity($data = null, array $options = [])
 * @method \Consolegst\Model\Entity\WsTblClkctrlHistoricoUnidade[] newEntities(array $data, array $options = [])
 * @method \Consolegst\Model\Entity\WsTblClkctrlHistoricoUnidade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Consolegst\Model\Entity\WsTblClkctrlHistoricoUnidade saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Consolegst\Model\Entity\WsTblClkctrlHistoricoUnidade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Consolegst\Model\Entity\WsTblClkctrlHistoricoUnidade[] patchEntities($entities, array $data, array $options = [])
 * @method \Consolegst\Model\Entity\WsTblClkctrlHistoricoUnidade findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WsTblClkctrlHistoricoUnidadesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ws_tbl_clkctrl_historico_unidades');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            // ->requirePresence('id', 'create')
            ->allowEmptyString('id', false);

        $validator
            ->scalar('block_desc')
            ->maxLength('block_desc', 55)
            ->allowEmptyString('block_desc');

        $validator
            ->allowEmptyString('status');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'gst';
    }
}
