<?php
namespace Consolegst\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Consolegst\Model\Table\WsTblClkctrlHistoricoUnidadesTable;

/**
 * Consolegst\Model\Table\WsTblClkctrlHistoricoUnidadesTable Test Case
 */
class WsTblClkctrlHistoricoUnidadesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Consolegst\Model\Table\WsTblClkctrlHistoricoUnidadesTable
     */
    public $WsTblClkctrlHistoricoUnidades;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Consolegst.WsTblClkctrlHistoricoUnidades'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('WsTblClkctrlHistoricoUnidades') ? [] : ['className' => WsTblClkctrlHistoricoUnidadesTable::class];
        $this->WsTblClkctrlHistoricoUnidades = TableRegistry::getTableLocator()->get('WsTblClkctrlHistoricoUnidades', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WsTblClkctrlHistoricoUnidades);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
