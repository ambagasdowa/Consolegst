<?php
namespace Consolegst\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Consolegst\Model\Table\WsTblGstCopilotoHistoricoUnidadesTable;

/**
 * Consolegst\Model\Table\WsTblGstCopilotoHistoricoUnidadesTable Test Case
 */
class WsTblGstCopilotoHistoricoUnidadesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Consolegst\Model\Table\WsTblGstCopilotoHistoricoUnidadesTable
     */
    public $WsTblGstCopilotoHistoricoUnidades;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Consolegst.WsTblGstCopilotoHistoricoUnidades'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('WsTblGstCopilotoHistoricoUnidades') ? [] : ['className' => WsTblGstCopilotoHistoricoUnidadesTable::class];
        $this->WsTblGstCopilotoHistoricoUnidades = TableRegistry::getTableLocator()->get('WsTblGstCopilotoHistoricoUnidades', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WsTblGstCopilotoHistoricoUnidades);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
