<?php
namespace Consolegst\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * WsTblClkctrlHistoricoUnidadesFixture
 */
class WsTblClkctrlHistoricoUnidadesFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 19, 'autoIncrement' => true, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'block_desc' => ['type' => 'string', 'fixed' => true, 'length' => 55, 'null' => true, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => 'getdate', 'precision' => null, 'comment' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'status' => ['type' => 'tinyinteger', 'length' => 3, 'null' => true, 'default' => '1', 'precision' => null, 'comment' => null, 'unsigned' => null],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'block_desc' => 'Lorem ipsum dolor sit amet',
                'created' => 1562353413,
                'modified' => 1562353413,
                'status' => 1
            ],
        ];
        parent::init();
    }
}
