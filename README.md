# Consolegst plugin for CakePHP

## Installation

You can install this plugin into your CakePHP application using [composer](https://getcomposer.org).

The recommended way to install composer packages is:

```
composer require your-name-here/Consolegst
```


# Consolegst plugin for CakePHP

## Installation

You can install this plugin into your CakePHP application using [composer](https://getcomposer.org).

The recommended way to install composer packages is:

```
composer require your-name-here/Consolegst
```

# Consolegst
Portable Pluging for common Project

### Enable Plugin

```php
// config/bootstrap.php
Plugin::load('Consolegst');
```

### Enable only clic
```php
  bin/cake plugin load --cli Consolegst
  bin/cake plugin unload --cli Consolegst
```

### Enable theme perController
> in controller add method
```php
public function beforeRender(Event $event) {
    $this->viewBuilder()->theme('Consolegst');
}
```

### Enable theme Global
```php
// src/Controller/AppController.php

public function beforeRender(Event $event) {
    $this->viewBuilder()->theme('Consolegst');
}
```

> Since handling static assets, such as images, JavaScript and CSS files of plugins,
through the Dispatcher is incredibly inefficient, it is strongly recommended to symlink them for production.
This can be done by using the plugin shell:
```php
bin/cake plugin assets symlink
bin/cake plugin assets symlink MyPlugin

```

### Add a repository as submodule
```bash
cd plugins
git submodule add https://gitlab.com/ambagasdowa/Consolegst.git
```
### Update as submodule

cd plugins and run

```bash
git submodule init
# and
git submodule update
```

OR using `--recurse-submodules`

```bash
  git clone --recurse-submodules https://gitlab.com/youraccount/yourRepo.git
```
OR Running master checkouts
Third-party components are handled as git submodules which have to be initialized first. So aside from the regular git checkout invoking

```git
  git submodule update --init
```

or a similar command is needed, for details see Git documentation.


### remove a submodule
```bash
rm -Rf Consolegst/

git rm -r Consolegst/
```
